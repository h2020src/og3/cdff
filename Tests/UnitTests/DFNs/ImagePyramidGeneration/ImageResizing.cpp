/* --------------------------------------------------------------------------
*
* (C) Copyright …
*
* ---------------------------------------------------------------------------
*/

/*!
 * @file ImageResizing.cpp
 * @date 19/02/2018
 * @author Alessandro Bianco
 */

/*!
 * @addtogroup DFNsTest
 *
 * Unit Test for the DFN ImageResizing.
 *
 *
 * @{
 */

/* --------------------------------------------------------------------------
 *
 * Includes
 *
 * --------------------------------------------------------------------------
 */
#include <catch.hpp>
#include <ImagePyramidGeneration/ImageResizing.hpp>
#include <Converters/MatToFrameConverter.hpp>
#include <Converters/FramePyramidToMatConverter.hpp>

using namespace CDFF::DFN::ImagePyramidGeneration;
using namespace Converters;
using namespace FrameWrapper;
using namespace FramePyramidWrapper;

/* --------------------------------------------------------------------------
 *
 * Test Cases
 *
 * --------------------------------------------------------------------------
 */
TEST_CASE( "Call to process (image resizing)", "[process]" )
{
	const unsigned defaultNumberOfLevels = 3;
	const double defaultRatio = 0.5;
	const unsigned imageWidth = 500;
	const unsigned imageHeight = 500;

	// Prepare input data
	cv::Mat inputImage(imageHeight, imageWidth, CV_8UC3, cv::Scalar(100, 100, 100));
	FrameConstPtr inputFrame = MatToFrameConverter().Convert(inputImage);

	// Instantiate DFN
	ImageResizing* generator = new ImageResizing;

	// Send input data to DFN
	generator->imageInput(*inputFrame);

	// Run DFN
	generator->process();

	// Query output data from DFN
	const FramePyramid& output = generator->imagePyramidOutput();
	REQUIRE(GetLevelRatio(output) == defaultRatio ); //This is the default level ratio;
	REQUIRE(GetNumberOfLevels(output) == defaultNumberOfLevels); //This is the default number of levels;
	
	unsigned expectedWidth = imageWidth;
	unsigned expectedHeight = imageHeight;	
	for (unsigned i=0; i<GetNumberOfLevels(output); i++)
		{
		const Frame& frame = GetFrame(output, i);
		REQUIRE(GetFrameWidth(frame) == expectedWidth);
		REQUIRE(GetFrameHeight(frame) == expectedHeight);

		expectedWidth = std::round( static_cast<double>(expectedWidth) * defaultRatio );
		expectedHeight = std::round( static_cast<double>(expectedHeight) * defaultRatio );
		}

	// Cleanup
	delete generator;
}

TEST_CASE( "Call to configure (image resizing)", "[configure]" )
{
	// Instantiate DFN
	ImageResizing* generator = new ImageResizing;

	// Setup DFN
	generator->setConfigurationFile("../tests/ConfigurationFiles/DFNs/ImagePyramidGeneration/ImageResizing_Conf1.yaml");
	generator->configure();

	// Cleanup
	delete generator;
}

/** @} */
