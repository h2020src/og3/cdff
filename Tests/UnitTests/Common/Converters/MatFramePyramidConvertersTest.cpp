/* --------------------------------------------------------------------------
*
* (C) Copyright …
*
* ---------------------------------------------------------------------------
*/

/*!
 * @file MatFramePyramidConvertersTest.cpp
 * @date 01/12/2017
 * @author Alessandro Bianco
 */

/*!
 * @addtogroup CommonTests
 * 
 * Testing conversion from Mat to FramePyramid and viceversa.
 * 
 * 
 * @{
 */

/* --------------------------------------------------------------------------
 *
 * Includes
 *
 * --------------------------------------------------------------------------
 */
#include <catch.hpp>
#include <Converters/FramePyramidToMatConverter.hpp>
#include <Converters/MatToFramePyramidConverter.hpp>
#include <Types/CPP/Frame.hpp>
#include <Types/CPP/FramePyramid.hpp>
#include <Errors/Assert.hpp>

using namespace Converters;
using namespace FramePyramidWrapper;
using namespace FrameWrapper;

TEST_CASE( "Vector Mat to Frame Pyramid and Back", "[VectorMatToFramePyramid]" )
	{
	MatToFramePyramidConverter firstConverter;
	FramePyramidToMatConverter secondConverter;

	cv::Mat inputMatrix1(500, 500, CV_8UC3, cv::Scalar(0,0,0));
	cv::Mat inputMatrix2(250, 250, CV_8UC3, cv::Scalar(0,0,0));
	for(int rowIndex = 0; rowIndex < inputMatrix1.rows; rowIndex++)
		{
		for(int columnIndex = 0; columnIndex < inputMatrix1.cols; columnIndex++)
			{
			inputMatrix1.at<cv::Vec3b>(rowIndex, columnIndex)[0] = rowIndex % 256;
			inputMatrix1.at<cv::Vec3b>(rowIndex, columnIndex)[1] = columnIndex % 256;
			inputMatrix1.at<cv::Vec3b>(rowIndex, columnIndex)[2] = (rowIndex + columnIndex) % 256;
			}
		}
	for(int rowIndex = 0; rowIndex < inputMatrix2.rows; rowIndex++)
		{
		for(int columnIndex = 0; columnIndex < inputMatrix2.cols; columnIndex++)
			{
			inputMatrix2.at<cv::Vec3b>(rowIndex, columnIndex)[0] = (rowIndex/2) % 256;
			inputMatrix2.at<cv::Vec3b>(rowIndex, columnIndex)[1] = (columnIndex/2) % 256;
			inputMatrix2.at<cv::Vec3b>(rowIndex, columnIndex)[2] = ((rowIndex + columnIndex)/2) % 256;
			}
		}
	std::vector<cv::Mat> inputVector{inputMatrix1, inputMatrix2};

	FramePyramidSharedConstPtr asnFramePyramid = firstConverter.ConvertShared(inputVector, 0.5);
	REQUIRE(GetNumberOfLevels(*asnFramePyramid) == static_cast<int>(inputVector.size()) );
	REQUIRE(GetLevelRatio(*asnFramePyramid) == 0.5 );
	for(unsigned i=0; i<inputVector.size(); i++)
		{
		const Frame& asnFrame = GetFrame(*asnFramePyramid, i);
		REQUIRE(GetFrameMode(asnFrame) == FrameWrapper::MODE_RGB );
		REQUIRE(GetNumberOfDataBytes(asnFrame) == inputVector.at(i).cols * inputVector.at(i).rows * 3);
		for(int byteIndex = 0; byteIndex < GetNumberOfDataBytes(asnFrame); byteIndex += 3)
			{
			int rowIndex = (byteIndex / 3) / inputVector.at(i).cols;
			int columnIndex = (byteIndex / 3) % inputVector.at(i).cols;
			REQUIRE(GetDataByte(asnFrame, byteIndex) == inputVector.at(i).at<cv::Vec3b>(rowIndex, columnIndex)[0] );
			REQUIRE(GetDataByte(asnFrame, byteIndex+1) == inputVector.at(i).at<cv::Vec3b>(rowIndex, columnIndex)[1] );				 		 
			REQUIRE(GetDataByte(asnFrame, byteIndex+2) == inputVector.at(i).at<cv::Vec3b>(rowIndex, columnIndex)[2] );
			}
		}

	std::vector<cv::Mat> outputVector = secondConverter.ConvertShared(asnFramePyramid);
	REQUIRE(outputVector.size() == inputVector.size());

	for(unsigned i=0; i<inputVector.size(); i++)
		{
		REQUIRE(outputVector.at(i).cols == inputVector.at(i).cols);
		REQUIRE(outputVector.at(i).type() == inputVector.at(i).type());
		for(int rowIndex = 0; rowIndex < inputVector.at(i).rows; rowIndex++)
			{
			for(int columnIndex = 0; columnIndex < inputVector.at(i).cols; columnIndex++)
				{
				cv::Vec3b& outputPixel = outputVector.at(i).at<cv::Vec3b>(rowIndex, columnIndex);
				cv::Vec3b& inputPixel = inputVector.at(i).at<cv::Vec3b>(rowIndex, columnIndex);
				REQUIRE(inputPixel[0] == outputPixel[0]);	
				REQUIRE(inputPixel[1] == outputPixel[1]);			 
				REQUIRE(inputPixel[2] == outputPixel[2]);			 		 
				}
			}
		}

	asnFramePyramid.reset();
	} 

TEST_CASE( "Vector Mat to Frame Pyramid Empty", "[VectorMatToFramePyramidEmpty]" )
	{
	MatToFramePyramidConverter firstConverter;
	FramePyramidToMatConverter secondConverter;

	std::vector<cv::Mat> inputVector;
	FramePyramidSharedConstPtr asnFramePyramid = firstConverter.ConvertShared(inputVector, 0.5);
	REQUIRE(GetNumberOfLevels(*asnFramePyramid) == static_cast<int>(inputVector.size()) );

	std::vector<cv::Mat> outputVector = secondConverter.ConvertShared(asnFramePyramid);
	REQUIRE(outputVector.size() == inputVector.size());
	}

