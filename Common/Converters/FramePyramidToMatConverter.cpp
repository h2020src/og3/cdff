/* --------------------------------------------------------------------------
*
* (C) Copyright …
*
* ---------------------------------------------------------------------------
*/

/*!
 * @file FramePyramidToMatConverter.cpp
 * @date 14/04/2018
 * @author Alessandro Bianco  and  Nassir W. Oumer 
 */

/*!
 * @addtogroup Converters
 * 
 * Implementation of FramePyramidToMatConverter.
 * 
 * 
 * @{
 */

/* --------------------------------------------------------------------------
 *
 * Includes
 *
 * --------------------------------------------------------------------------
 */

#include "FramePyramidToMatConverter.hpp"
#include "FrameToMatConverter.hpp"
#include <Errors/Assert.hpp>
#include <iostream>

namespace Converters {

using namespace FramePyramidWrapper;

/* --------------------------------------------------------------------------
 *
 * Public Member Functions
 *
 * --------------------------------------------------------------------------
 */

const std::vector<cv::Mat> FramePyramidToMatConverter::Convert(const FramePyramidWrapper::FramePyramidConstPtr& framePyramid)
	{
	FrameToMatConverter frameConverter;
	std::vector<cv::Mat> conversion;
	unsigned numberOfLevels = GetNumberOfLevels(*framePyramid);
	for(int i=0; i<numberOfLevels; i++)
		{
		const FrameWrapper::Frame& frame = GetFrame(*framePyramid, i);
		cv::Mat newFrame = frameConverter.Convert(&frame);
		conversion.push_back(newFrame);
		}
	return conversion;
	}


const std::vector<cv::Mat> FramePyramidToMatConverter::ConvertShared(const FramePyramidWrapper::FramePyramidSharedConstPtr& frame)
	{
	return Convert(frame.get());
	} 

}

/** @} */
