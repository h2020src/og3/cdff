/*!
 * @file MatToFramePyramidConverter.hpp
 * @date 04/09/2019
 * @author Alessandro Bianco
 */

/*!
 * @addtogroup Converters
 * 
 *  This is the class for type conversion from Mat to FramePyramid.
 *  
 *
 * @{
 */

#ifndef MAT_TO_FRAME_PYRAMID_CONVERTER_HPP
#define  MAT_TO_FRAME_PYRAMID_CONVERTER_HPP


/* --------------------------------------------------------------------------
 *
 * Includes
 *
 * --------------------------------------------------------------------------
 */
#include <Types/CPP/FramePyramid.hpp>
#include <opencv2/core/core.hpp>


namespace Converters {

/* --------------------------------------------------------------------------
 *
 * Class definition
 *
 * --------------------------------------------------------------------------
 */
class MatToFramePyramidConverter
	{
	/* --------------------------------------------------------------------
	 * Public
	 * --------------------------------------------------------------------
	 */
	public:
		virtual FramePyramidWrapper::FramePyramidConstPtr Convert(const std::vector<cv::Mat>& imageVector, double ratio);
		FramePyramidWrapper::FramePyramidSharedConstPtr ConvertShared(const std::vector<cv::Mat>& imageVector, double ratio);

	/* --------------------------------------------------------------------
	 * Protected
	 * --------------------------------------------------------------------
	 */
        protected:

	/* --------------------------------------------------------------------
	 * Private
	 * --------------------------------------------------------------------
	 */	
	private:

	};

}

#endif

/* MatToFramePyramidConverter.hpp */
/** @} */
