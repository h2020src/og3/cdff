/* --------------------------------------------------------------------------
*
* (C) Copyright …
*
* --------------------------------------------------------------------------
*/

/*!
 * @file FramePyramidToMatConverter.hpp
 * @date 08/12/2017
 * @author Alessandro Bianco
 */

/*!
 * @addtogroup Converters
 * 
 *  This is the class for type conversion from FramePyramid to Mat.
 *  
 *
 * @{
 */

#ifndef FRAME_PYRAMID_TO_MAT_CONVERTER_HPP
#define FRAME_PYRAMID_TO_MAT_CONVERTER_HPP


/* --------------------------------------------------------------------------
 *
 * Includes
 *
 * --------------------------------------------------------------------------
 */
#include <Types/CPP/FramePyramid.hpp>
#include <opencv2/core/core.hpp>

namespace Converters {

/* --------------------------------------------------------------------------
 *
 * Class definition
 *
 * --------------------------------------------------------------------------
 */
class FramePyramidToMatConverter
	{
	/* --------------------------------------------------------------------
	 * Public
	 * --------------------------------------------------------------------
	 */
	public:
		virtual const std::vector<cv::Mat> Convert(const FramePyramidWrapper::FramePyramidConstPtr& framePyramid);
		const std::vector<cv::Mat> ConvertShared(const FramePyramidWrapper::FramePyramidSharedConstPtr& framePyramid);

	/* --------------------------------------------------------------------
	 * Protected
	 * --------------------------------------------------------------------
	 */
        protected:

	/* --------------------------------------------------------------------
	 * Private
	 * --------------------------------------------------------------------
	 */	
	private:

	};

}

#endif

/* FramePyramidToMatConverter.hpp */
/** @} */
