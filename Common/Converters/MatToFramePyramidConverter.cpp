/*!
 * @file MatToFramePyramidConverter.cpp
 * @date 04/09/2019
 * @authors Alessandro Bianco
 */

/*!
 * @addtogroup Converters
 * 
 * Implementation of MatToFramePyramidConverter class.
 * 
 * 
 * @{
 */

/* --------------------------------------------------------------------------
 *
 * Includes
 *
 * --------------------------------------------------------------------------
 */

#include "MatToFramePyramidConverter.hpp"
#include "MatToFrameConverter.hpp"
#include <Errors/Assert.hpp>

namespace Converters {

using namespace FramePyramidWrapper;

/* --------------------------------------------------------------------------
 *
 * Public Member Functions
 *
 * --------------------------------------------------------------------------
 */

FramePyramidConstPtr MatToFramePyramidConverter::Convert(const std::vector<cv::Mat>& imageVector, double ratio)
	{
	MatToFrameConverter frameConverter;
	FramePyramidPtr framePyramid = FramePyramidWrapper::NewFramePyramid();
	int numberOfLevels = imageVector.size();	
	
	for(int i=0; i< numberOfLevels; i++)
		{
		ASSERT( 
			i==0 || 
				(
				std::abs(static_cast<double>(imageVector.at(i-1).rows) * ratio - static_cast<double>(imageVector.at(i).rows)) < 1 &&
				std::abs(static_cast<double>(imageVector.at(i-1).cols) * ratio - static_cast<double>(imageVector.at(i).cols)) < 1
				),
				"Error when coverting to FramePyramids: input images do not respect ratio"
			);
		FrameWrapper::FrameConstPtr newFrame = frameConverter.Convert(imageVector.at(i));
		AddFrame(*framePyramid, *newFrame);
		}

	SetLevelRatio(*framePyramid, ratio);
	return framePyramid;
	}


FramePyramidSharedConstPtr MatToFramePyramidConverter::ConvertShared(const std::vector<cv::Mat>& imageVector, double ratio)
	{
	FramePyramidConstPtr frame = Convert(imageVector, ratio);
	FramePyramidSharedConstPtr sharedFramePyramid(frame);
	return sharedFramePyramid;
	}

}

/** @} */
