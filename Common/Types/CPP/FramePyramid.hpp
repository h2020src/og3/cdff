/**
 * @author Alessandro Bianco
 */

/**
 * @addtogroup FramePyramidWrapper
 *
 * Wrapper for ASN.1 FramePyramid type
 *
 * @{
 */

#ifndef FRAME_PYRAMID_HPP
#define FRAME_PYRAMID_HPP

#include <Types/C/FramePyramid.h>
#include <Types/C/Array3D.h>
#include "Array3D.hpp"
#include "Frame.hpp"
#include <Types/C/taste-extended.h>

#include "BaseTypes.hpp"
#include <cstdlib>
#include <memory>
#include "Errors/AssertOnTest.hpp"
#include <cstring>

namespace FramePyramidWrapper
{

// Global constant variables

const BaseTypesWrapper::T_UInt16 PYRAMID_MAX_LEVELS = pyramidMaxLevels;

//Types
typedef asn1SccFramePyramid FramePyramid;

// Pointer types

typedef FramePyramid* FramePyramidPtr;
typedef FramePyramid const* FramePyramidConstPtr;
typedef std::shared_ptr<FramePyramid> FramePyramidSharedPtr;
typedef std::shared_ptr<const FramePyramid> FramePyramidSharedConstPtr;

// Constructors

FramePyramidPtr NewFramePyramid();
FramePyramidSharedPtr NewSharedFramePyramid();
FramePyramidConstPtr Clone(const FramePyramid& source);
FramePyramidSharedPtr SharedClone(const FramePyramid& source);

// Initializers

void Initialize(FramePyramid& framePyramid);
void Copy(const FramePyramid& source, FramePyramid& destination);
void Clear(FramePyramid& framePyramid);

// Data Access functions

void SetNumberOfLevels(FramePyramid& framePyramid, BaseTypesWrapper::T_UInt16 numberOfLevels);
BaseTypesWrapper::T_UInt16 GetNumberOfLevels(const FramePyramid& framePyramid);

void SetLevelRatio(FramePyramid& framePyramid, BaseTypesWrapper::T_Double levelRatio);
BaseTypesWrapper::T_Double GetLevelRatio(const FramePyramid& framePyramid);

void AddFrame(FramePyramid& framePyramid, const FrameWrapper::Frame& frame);
const FrameWrapper::Frame& GetFrame(const FramePyramid& framePyramid, BaseTypesWrapper::T_UInt32 frameIndex);

// Bitstream conversion functions

BitStream ConvertToBitStream(const FramePyramid& framePyramid);
void ConvertFromBitStream(BitStream bitStream, FramePyramid& framePyramid);

}

#endif // FRAME_PYRAMID_HPP

/** @} */
