/**
 * @author Alessandro Bianco
 */

/**
 * @addtogroup FramePyramidWrapper
 * @{
 */

#include "FramePyramid.hpp"

namespace FramePyramidWrapper
{

using namespace BaseTypesWrapper;
using namespace FrameWrapper;

FramePyramidPtr NewFramePyramid()
{
	FramePyramidPtr frame = new FramePyramid();
	Initialize(*frame);
	return frame;
}

FramePyramidSharedPtr NewSharedFramePyramid()
{
	FramePyramidSharedPtr sharedFramePyramid = std::make_shared<FramePyramid>();
	Initialize(*sharedFramePyramid);
	return sharedFramePyramid;
}

FramePyramidConstPtr Clone(const FramePyramid& source)
{
	FramePyramidPtr frame = new FramePyramid();
	Copy(source, *frame);
	return frame;
}

FramePyramidSharedPtr SharedClone(const FramePyramid& source)
{
	FramePyramidSharedPtr sharedFramePyramid = std::make_shared<FramePyramid>();
	Copy(source, *sharedFramePyramid);
	return sharedFramePyramid;
}

void Initialize(FramePyramid& framePyramid)
{
	Clear(framePyramid);
}

void Copy(const FramePyramid& source, FramePyramid& destination)
{
	Clear(destination);
	destination.ratio = source.ratio;
	for(unsigned i=0; i<source.levels; i++)
		{
		AddFrame(destination, GetFrame(source, i));
		}
}

void Clear(FramePyramid& framePyramid)
{
	framePyramid.levels = 0;
	framePyramid.ratio = 1;
}

void SetNumberOfLevels(FramePyramid& framePyramid, BaseTypesWrapper::T_UInt16 numberOfLevels)
{
	framePyramid.levels = numberOfLevels;
}

BaseTypesWrapper::T_UInt16 GetNumberOfLevels(const FramePyramid& framePyramid)
{
	return framePyramid.levels;
}

void SetLevelRatio(FramePyramid& framePyramid, BaseTypesWrapper::T_Double levelRatio)
{
	framePyramid.ratio = levelRatio;
}

BaseTypesWrapper::T_Double GetLevelRatio(const FramePyramid& framePyramid)
{
	return framePyramid.ratio;
}

void AddFrame(FramePyramid& framePyramid, const Frame& frame)
{
	ASSERT( framePyramid.levels < PYRAMID_MAX_LEVELS, "Maximum number of levels in pyramid was reached.");
	FrameWrapper::Copy(frame, framePyramid.frames.arr[framePyramid.levels]);
	framePyramid.levels++;
}

const Frame& GetFrame(const FramePyramid& framePyramid, BaseTypesWrapper::T_UInt32 frameIndex)
{
	return framePyramid.frames.arr[frameIndex];
}

BitStream ConvertToBitStream(const FramePyramid& frame)
	{
	return BaseTypesWrapper::ConvertToBitStream(frame, asn1SccFramePyramid_REQUIRED_BYTES_FOR_ENCODING, asn1SccFramePyramid_Encode);
	}

void ConvertFromBitStream(BitStream bitStream, FramePyramid& frame)
	{
	BaseTypesWrapper::ConvertFromBitStream(bitStream, asn1SccFramePyramid_REQUIRED_BYTES_FOR_ENCODING, frame, asn1SccFramePyramid_Decode);
	}

}

/** @} */
