/**
 * @addtogroup DFNs
 * @{
 */

#include "ImagePyramidGenerationInterface.hpp"

namespace CDFF
{
namespace DFN
{

ImagePyramidGenerationInterface::ImagePyramidGenerationInterface()
{
    asn1SccFrame_Initialize(& inImage);
    asn1SccFramePyramid_Initialize(& outImagePyramid);
}

ImagePyramidGenerationInterface::~ImagePyramidGenerationInterface()
{
}

void ImagePyramidGenerationInterface::imageInput(const asn1SccFrame& data)
{
    inImage = data;
}

const asn1SccFramePyramid& ImagePyramidGenerationInterface::imagePyramidOutput() const
{
    return outImagePyramid;
}

}
}

/** @} */
