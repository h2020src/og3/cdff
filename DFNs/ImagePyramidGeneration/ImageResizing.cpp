/**
 * @author Alessandro Bianco
 */

/**
 * @addtogroup DFNs
 * @{
 */

#include "ImageResizing.hpp"

#include <Errors/Assert.hpp>
#include <Macros/YamlcppMacros.hpp>

#include <cstdlib>
#include <fstream>

using namespace Converters;
using namespace FrameWrapper;
using namespace FramePyramidWrapper;

namespace CDFF
{
namespace DFN
{
namespace ImagePyramidGeneration
{

ImageResizing::ImageResizing()
{
        parameters = DEFAULT_PARAMETERS;

	parametersHelper.AddParameter<int>("GeneralParameters", "NumberOfLevels", parameters.numberOfLevels, DEFAULT_PARAMETERS.numberOfLevels);
	parametersHelper.AddParameter<double>("GeneralParameters", "LevelRatio", parameters.levelRatio, DEFAULT_PARAMETERS.levelRatio);
	parametersHelper.AddParameter<InterpolationMethod, InterpolationMethodHelper>("GeneralParameters", "InterpolationMethod", parameters.interpolationMethod, 
		DEFAULT_PARAMETERS.interpolationMethod);

	configurationFilePath = "";
}

ImageResizing::~ImageResizing()
{
}

ImageResizing::InterpolationMethodHelper::InterpolationMethodHelper
	(const std::string& parameterName, InterpolationMethod& boundVariable, const InterpolationMethod& defaultValue) :
	ParameterHelper(parameterName, boundVariable, defaultValue)
{
}

ImageResizing::InterpolationMethod ImageResizing::InterpolationMethodHelper::Convert(const std::string& interpolationMethod)
{
	if (interpolationMethod == "Nearest" || interpolationMethod == "0")
	{
		return cv::INTER_NEAREST;
	}
	else if (interpolationMethod == "Bilinear" || interpolationMethod == "1")
	{
		return cv::INTER_LINEAR;
	}
	else if (interpolationMethod == "Bicubic" || interpolationMethod == "2")
	{
		return cv::INTER_CUBIC;
	}
	else if (interpolationMethod == "Resampling" || interpolationMethod == "3")
	{
		return cv::INTER_AREA;
	}
	else if (interpolationMethod == "Lanczos" || interpolationMethod == "4")
	{
		return cv::INTER_LANCZOS4;
	}
	else if (interpolationMethod == "ExactBilinear" || interpolationMethod == "5")
	{
		return cv::INTER_LINEAR_EXACT;
	}
	else if (interpolationMethod == "Max" || interpolationMethod == "6")
	{
		return cv::INTER_MAX;
	}
	else if (interpolationMethod == "FillOutliers" || interpolationMethod == "7")
	{
		return cv::WARP_FILL_OUTLIERS;
	}
	else if (interpolationMethod == "InverseMap" || interpolationMethod == "8")
	{
		return cv::WARP_INVERSE_MAP;
	}
	else
	{
		std::string errorString = "ImageResizing ConfigurationError: interpolation method has to be one of ";
		errorString += "{Nearest, Bilinear, Bicubic, Resampling, Lanczos, ExactBilinear, Max, FillOutliers, or InverseMap}";
		ASSERT(false, errorString);
	}
}

void ImageResizing::configure()
{
	parametersHelper.ReadFile(configurationFilePath);
	ValidateParameters();
}

void ImageResizing::process()
{
	// Read data from input port
	cv::Mat inputImage = frameToMat.Convert(&inImage);

	// Process data
	ValidateInputs(inputImage);
	std::vector<cv::Mat> imagePyramid = GenerateImagePyramid(inputImage);

	// Write data to output port
	FramePyramidConstPtr tmp = matToFramePyramid.Convert(imagePyramid, parameters.levelRatio);
	Copy(*tmp, outImagePyramid);
	delete(tmp);
}

const ImageResizing::ImageResizingOptionsSet ImageResizing::DEFAULT_PARAMETERS =
{
	/*.numberOfLevels =*/ 3,
	/*.levelRatio =*/ 0.5,
	/*.interpolationMethod =*/ cv::INTER_LINEAR
};

std::vector<cv::Mat> ImageResizing::GenerateImagePyramid(cv::Mat inputImage)
{
	std::vector<cv::Mat> imagePyramid;
	cv::Mat lastLevel = inputImage;

	imagePyramid.push_back(lastLevel);
	for(int i=1; i<parameters.numberOfLevels; i++)
		{
		cv::Size newLevelSize(std::round(lastLevel.cols*parameters.levelRatio), std::round(lastLevel.rows*parameters.levelRatio));
		cv::resize(inputImage, lastLevel, newLevelSize, parameters.interpolationMethod);
		imagePyramid.push_back(lastLevel);
		}

	return imagePyramid;
}

void ImageResizing::ValidateParameters()
{
	const std::string maximumLevelsErrorMessage = "Pyramid Generation Error: number of levels should be in [1-"+ std::to_string(FramePyramidWrapper::PYRAMID_MAX_LEVELS) + "]";
	ASSERT(parameters.numberOfLevels > 0 && parameters.numberOfLevels <= static_cast<int>(FramePyramidWrapper::PYRAMID_MAX_LEVELS), maximumLevelsErrorMessage);
	ASSERT(parameters.levelRatio > 0 && parameters.levelRatio <= 1.0, "Pyramid Generation Error: level ratio should be betwee 0 and 1");
}

void ImageResizing::ValidateInputs(cv::Mat inputImage)
{
	ASSERT(inputImage.type() == CV_8UC3 || inputImage.type() == CV_8UC1, "Image Undistortion error: input image is not of type CV_8UC3 or CV_8UC1");
	ASSERT(inputImage.rows > 0 && inputImage.cols > 0, "Image Undistortion error: input image is empty");
}

}
}
}

/** @} */
