/**
 * @addtogroup DFNs
 * @{
 */

#ifndef IMAGEPYRAMIDGENERATION_IMAGEPYRAMIDGENERATIONINTERFACE_HPP
#define IMAGEPYRAMIDGENERATION_IMAGEPYRAMIDGENERATIONINTERFACE_HPP

#include "DFNCommonInterface.hpp"
#include <Types/C/Frame.h>
#include <Types/C/FramePyramid.h>

namespace CDFF
{
namespace DFN
{
    /**
     * DFN that generates an image pyramid from a single image
     */
    class ImagePyramidGenerationInterface : public DFNCommonInterface
    {
        public:

            ImagePyramidGenerationInterface();
            virtual ~ImagePyramidGenerationInterface();

            /**
             * Send value to input port "image"
             * @param image
             *     2D image captured by a camera
             */
            virtual void imageInput(const asn1SccFrame& data);

            /**
             * Query value from output port "image"
             * @return image
             *     an image pyramid extracted from the image
             */
            virtual const asn1SccFramePyramid& imagePyramidOutput() const;

        protected:

            asn1SccFrame inImage;
            asn1SccFramePyramid outImagePyramid;
    };
}
}

#endif // IMAGEPYRAMIDGENERATION_IMAGEPYRAMIDGENERATIONINTERFACE_HPP

/** @} */
