/**
 * @author Alessandro Bianco
 */

/**
 * @addtogroup DFNs
 * @{
 */

#ifndef IMAGEPYRAMIDGENERATION_IMAGERESIZING_HPP
#define IMAGEPYRAMIDGENERATION_IMAGERESIZING_HPP

#include "ImagePyramidGenerationInterface.hpp"

#include <Types/CPP/Frame.hpp>
#include <Converters/FrameToMatConverter.hpp>
#include <Converters/MatToFramePyramidConverter.hpp>
#include <Helpers/ParametersListHelper.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <yaml-cpp/yaml.h>

namespace CDFF
{
namespace DFN
{
namespace ImagePyramidGeneration
{
	/**
	 * Generates imagePyramid (resizing function provided by OpenCV)
	 *
	 * @param numberOfLevels
	 *        Number of levels of the image pyramid;
	 * @param levelRatio
	 *	  the ratio by which the image dimension is multiplied at each level, must be stricly between 0 and 1;
	 * @param interpolationMethod
	 *	  the interpolation method used to approximate pixels during reduction, it can be one of
	 *	  Nearest, Bilinear, Bicubic, Resampling, Lanczos, ExactBilinear, Max, FillOutliers, or InverseMap.
	 */
	class ImageResizing : public ImagePyramidGenerationInterface
	{
		public:

			ImageResizing();
			virtual ~ImageResizing();

			virtual void configure() override;
			virtual void process() override;

	private:

		typedef cv::InterpolationFlags InterpolationMethod;

		class InterpolationMethodHelper : public Helpers::ParameterHelper<InterpolationMethod, std::string>
		{
			public:
				InterpolationMethodHelper(const std::string& parameterName, InterpolationMethod& boundVariable, const InterpolationMethod& defaultValue);
			private:
				InterpolationMethod Convert(const std::string& value) override;
		};

		//DFN Parameters 
		struct ImageResizingOptionsSet
		{
			int numberOfLevels;
			double levelRatio;
			InterpolationMethod interpolationMethod;
		};

		Helpers::ParametersListHelper parametersHelper;
		ImageResizingOptionsSet parameters;
		static const ImageResizingOptionsSet DEFAULT_PARAMETERS;

		//External conversion helpers
		Converters::FrameToMatConverter frameToMat;
		Converters::MatToFramePyramidConverter matToFramePyramid;

		//Core computation methods
		std::vector<cv::Mat>  GenerateImagePyramid(cv::Mat inputImage);

		//Input Validation methods
		void ValidateParameters();
		void ValidateInputs(cv::Mat inputImage);
	};
}
}
}

#endif // IMAGEPYRAMIDGENERATION_IMAGERESIZING_HPP

/** @} */
