#!/usr/bin/env bash

# This script removes from Memcheck's XML report all the memory errors that we
# are not interested in, that is to say, the errors whose stack trace does not
# hit any CDFF function, for instance stack traces that are entirely in the
# CDFF's dependencies or in the C++ standard library, and that Memcheck still
# reports for some reason.
#
# Memchek is Valgrind's default tool.
#
# USAGE
#
# ./purge-memcheck-report.sh /path/to/memchek/report.xml
#
# DEPENDENCIES
#
# GNU coreutils, GNU grep, structured grep (apt:coreutils grep sgrep)
#
# https://www.cs.helsinki.fi/u/jjaakkol/sgrep.html
# https://www.cs.helsinki.fi/u/jjaakkol/sgrepman.html
#
# KNOWN BUGS
#
# The error count is wrong since errors are excluded: don't mind it.
#
# This script shouldn't even exist: you should use a Valgrind suppression file
# instead (however, it is less easy to write).
#
# MAINTAINER
#
# Romain Michalec

# Prefix for status and error messages

MSG_PREFIX="CDFF Memcheck report postprocessor:"

# Check input argument

if [[ ! -f "${1}" ]]
then
  echo "${MSG_PREFIX} Cannot open file '${1}': no such file"
  exit 1
elif [[ ! (-r "${1}" && -w "${1}") ]]
then
  echo "${MSG_PREFIX} Cannot open file '${1}': file must be readable and writable"
  exit 1
elif [[ ! -w "$(dirname "${1}")" ]]
then
  echo "${MSG_PREFIX} Cannot write to directory '$(dirname "${1}")': directory must be writable"
  exit 1
elif ! grep -e "<error>" "${1}"
then
  exit 0 # no error in input file: nothing to do, exit silently
fi

# Extract beginning of report

sgrep -n -N -e 'start ._ "<error>"' "${1}" >"${1}.preamble"

# Extract errors whose stack trace features functions from files in directories
# under cdff/, with the exception of the following excluded directories:
#
# * cdff/Tests/UnitTests/Catch
#   The unit test library, a dependency of the CDFF
#
# * cdff/Common/Core/liborbslam
#   A fork of ORB-SLAM2, a dependency of the CDFF

sgrep -n -o '%r\n\n' -e '
  "<error>" .. "</error>" containing
  (
    "<stack>" .. "</stack>" containing
    (
      "<dir>" .. "</dir>" containing
      (
        "/builds/h2020src/og3/cdff" not in (
          "/builds/h2020src/og3/cdff/Tests/UnitTests/Catch" or "/builds/h2020src/og3/cdff/Common/Core/liborbslam"
        )
      )
    )
  )' \
  "${1}" >"${1}.errors"

# Extract error count

sgrep -n -e '"<errorcounts>" .. end' "${1}" >"${1}.errorcount"

# Fix the error count: only keep count of the errors that were previously
# extracted
#
# KNOWN BUG
#
# The following quick and easy implementation parses the list of extracted
# errors to limit the error count to those errors, however the error count is
# still wrong for those errors, because they aren't recounted

printf "<errorcounts>\n" >"${1}.errorcount.fixed"
grep -B 2 -A 1 -F \
     -e "$(grep -o -e '<unique>.*</unique>' "${1}.errors" | tac)" \
     "${1}.errorcount" | grep -v -e "--" >>"${1}.errorcount.fixed"
sgrep -n -N -e '"</errorcounts>" .. end' "${1}.errorcount" >>"${1}.errorcount.fixed"

## Overwrite input file and clean up

cp "${1}" "${1}.bak" # TMP

cat "${1}.preamble" "${1}.errors" "${1}.errorcount.fixed" >"${1}"
rm  "${1}.preamble" "${1}.errors" "${1}.errorcount.fixed" "${1}.errorcount"
