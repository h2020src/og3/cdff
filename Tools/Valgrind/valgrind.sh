#!/usr/bin/env bash

# This script is a wrapper around "valgrind cdff-unit-tests", that is to say a
# wrapper around a run of the CDFF's unit tests through Valgrind's default tool,
# Memcheck, a memory error detector.
#
# USAGE
#
# ./valgrind.sh [options] [program]
#
# The default client program is /path/to/CDFF/build/.../cdff-unit-tests.
#
# For CI, this script must be run in a Docker container where Valgrind is
# available and where the ASN.1 datatypes in the CDFF's source tree are
# already transcompiled to C code.
#
# OPTIONS
#
# --hardcore
#     Report all memory leaks. The default is to report definitely lost blocks
#     and possibly lost blocks, merge multiple leaks into a single leak report
#     when the first four entries in the backtrace match, and postprocess the
#     final XML error report with /path/to/CDFF/Tools/Valgrind/purge-memcheck-
#     report.sh (see that file for information). If this option is provided,
#     also report indirectly lost blocks and still reachable blocks, merge
#     multiple leaks only when all the entries in the backtrace match, and don't
#     run the final XML error report through the postprocessor.
#
# --fast
#     Don't track the origin of uninitialised values, in undefined value errors.
#     The default is to track them, which is expensive: it halves Memcheck's
#     speed and increases memory use by a minimum of 100 MB, and possibly more.
#     Nevertheless it can drastically reduce the effort required to identify the
#     root cause of uninitialised value errors, and so is often a programmer
#     productivity win, despite running more slowly.
#
# --xml-file=<filename>
#     Report memory errors in <filename>, default /path/to/CDFF/Tools/Valgrind/
#     report/valgrind.xml.
#
# --log-file=<filename>
#     Write less important plaintext messages, if any, in <filename>, default
#     /path/to/CDFF/Tools/Valgrind/report/valgrind.log.
#
# --suppressions=<filename>
#     Use Valgrind suppression file <filename>, default /path/to/CDFF/Tools/
#     Valgrind/suppressions.txt.
#
# DEPENDENCIES
#
# GNU coreutils, util-linux, GNU grep, Valgrind (apt:coreutils util-linux grep
# valgrind)
#
# KNOWN BUGS
#
# When using the default client program, any path given to --xml-file,
# --log-file, and --suppressions must be absolute or relative to the directory
# where the default client program is located (as opposed to relative to the
# current directory, which is normally the case). This is because the code of
# the default client program contains many hardcoded relative paths and as a
# result it must be run from the directory where it is located.
#
# There isn't any check on access rights: you must have write access on the
# directory where the report file will be written.
#
# SEE ALSO
#
# http://valgrind.org/docs/manual/QuickStart.html
# http://valgrind.org/docs/manual/manual.html
# http://valgrind.org/docs/manual/FAQ.html
#
# MAINTAINERS
#
# Xavier Martinez-Gonzalez, Romain Michalec

# Prefix for status and error messages
MSG_PREFIX="CDFF dynamic code analysis:"

# Canonical path to the directory containing this script
# Uses GNU readlink from GNU coreutils
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Default options for this script
HARDCORE=no
FAST=no
REPORT="${DIR}/report/valgrind.xml"
LOGFILE="${DIR}/report/valgrind.log"
SUPPRESSIONS="${DIR}/suppressions.txt"

# Parse command options (adapted from /usr/share/doc/util-linux/examples/
# getopt-parse.bash)
# Uses GNU getopt from util-linux, not the shell builtin getopts, nor the
# original getopt utility from the 1980s
LONG=hardcore,fast,xml-file:,log-file:,suppressions:
PARSED=$(getopt --options "" --longoptions ${LONG} --name "${0}" -- "${@}")
EXIT_STATUS=${?}
if [[ ${EXIT_STATUS} != 0 ]]
then
    echo "${MSG_PREFIX} Returning getopt's exit status" >&2
    exit ${EXIT_STATUS}
fi

eval set -- "${PARSED}"
while true
do
    case "${1}" in

      --hardcore) HARDCORE=yes; shift ;;
      --fast) FAST=yes; shift ;;

      --xml-file) REPORT="${2}"; shift 2 ;;
      --log-file) LOGFILE="${2}"; shift 2 ;;
      --suppressions) SUPPRESSIONS="${2}"; shift 2 ;;

      --) shift; break ;;
      *)  echo "${MSG_PREFIX} Internal error!" >&2; exit 1 ;;

    esac
done

# Client program
if [[ -f "${1}" && -x "${1}" ]]
then
    # Either given as an optional argument
    PROGRAM="${1}"
else
    # Or the default: the unit test executable
    PROGRAM="$(readlink -m "${DIR}/../../build/Tests/UnitTests/cdff-unit-tests")"
    # FIXME: see KNOWN BUGS
    cd "$(dirname "${PROGRAM}")"
fi

# Valgrind and Memcheck options
VALGRIND_FLAGS="--xml=yes --xml-file=\"${REPORT}\" --child-silent-after-fork=yes --fullpath-after=cdff/ --log-file=\"${LOGFILE}\" --suppressions=\"${SUPPRESSIONS}\""

if [[ "${HARDCORE}" != yes ]] # default
then
    MEMCHECK_FLAGS="--leak-check=full --leak-resolution=low --show-leak-kinds=definite,possible"
else
    MEMCHECK_FLAGS="--leak-check=full --leak-resolution=high --show-leak-kinds=all"
fi

if [[ "${FAST}" != yes ]] # default
then
    MEMCHECK_FLAGS+=" --track-origins=yes"
fi

# Run the client program through Valgrind
echo "${MSG_PREFIX} Running Valgrind (Memcheck) as follows:\
 valgrind --tool=memcheck ${VALGRIND_FLAGS} ${MEMCHECK_FLAGS} \"./$(basename "${PROGRAM}")\""

mkdir -p "$(dirname "${REPORT}")"
mkdir -p "$(dirname "${LOGFILE}")"
eval "valgrind --tool=memcheck ${VALGRIND_FLAGS} ${MEMCHECK_FLAGS} \"./$(basename "${PROGRAM}")\""

# Report on the success of memory error detection
EXIT_STATUS=${?}
if [[ ${EXIT_STATUS} != 0 ]]
then
    # Explain that memory error detection didn't terminate successfully
    echo "${MSG_PREFIX} Valgrind or its client program exited with exit status ${EXIT_STATUS}, memory error detection is probably incomplete"
    echo "${MSG_PREFIX} Memory errors written to file: ${REPORT}"
    echo "${MSG_PREFIX} Less important messages written to file: ${LOGFILE}"
    echo "${MSG_PREFIX} Unsuccessful"
    exit ${EXIT_STATUS}
else
    # Postprocess error report
    if [[ "${HARDCORE}" != yes ]] # default
    then
        "${DIR}/purge-memcheck-report" "${REPORT}"
    fi
    # Print how many memory errors were detected
    ERROR_COUNT=$(grep --count "<error>" "${REPORT}")
    echo "${MSG_PREFIX} Memory errors detected in the code that was run: ${ERROR_COUNT}"
    echo "${MSG_PREFIX} Memory errors written to file: ${REPORT}"
    echo "${MSG_PREFIX} Less important messages written to file: ${LOGFILE}"
    echo "${MSG_PREFIX} Done"
    exit ${ERROR_COUNT}
fi
